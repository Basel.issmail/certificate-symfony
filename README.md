Electronics go certificate
========================

Admin dashboard for https://electronics-go.com/ to generate electronics go initiative certificates.   
Generated certificates can be accessed with QR code and can be downloaded as pdf.

Example of generated certificate:    
https://electronics-go.com/certificate/phh57m-2auz60sig8h1
