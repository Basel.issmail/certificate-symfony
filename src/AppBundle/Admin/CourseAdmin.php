<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 02/09/18
 * Time: 11:27 م
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CourseAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('description')
            ->add('courseDate')
            ->add('certificateDate')
            //->add('attendees', 'sonata_type_collection')
            ->add('attendees', 'sonata_type_model', array(
                'multiple' => true,
                'by_reference' => false
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title')
            ->add('description')
            ->add('courseDate')
            ->add('certificateDate')
            ->add('attendees');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('title')
            ->add('description')
            ->add('courseDate')
            ->add('certificateDate')
            ->add('attendees', 'sonata_type_collection');
    }
}