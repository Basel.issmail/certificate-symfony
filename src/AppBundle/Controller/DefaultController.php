<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/{id}", name="certificate")
     */
    public function indexAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $urlIds = explode('-', $id);
        if(count($urlIds) != 2){
            return $this->render('@Twig/Exception/error.html.twig');
        }
        $course = $em->getRepository('AppBundle:Course')->findOneByUrlID($urlIds[0]);
        if($course == null || !$course){
            return $this->render('@Twig/Exception/error.html.twig');
        }

        /*dump($course->getAttendees());die();*/
        $attendeeUrlId = $urlIds[1];

        $attendee = null;
        foreach($course->getAttendees() as $attendeeItem) {
            if ($attendeeUrlId == $attendeeItem->getUrlId()) {
                $attendee = $attendeeItem;
                break;
            }
        }

        if($attendee == null || !$attendee){
            return $this->render('@Twig/Exception/error.html.twig');
        }

        return $this->render('certificate/certificate.html.twig', array(
            'attendee' => $attendee,
            'course' => $course
        ));
    }
}
