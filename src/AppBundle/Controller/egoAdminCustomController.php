<?php
/**
 * Created by PhpStorm.
 * User: basel
 * Date: 04/09/18
 * Time: 05:20 م
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class egoAdminCustomController extends Controller
{
    /**
     * @Route("/admin/app/courses", name="course_list")
     */
    public function coursesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $courses = $em->getRepository('AppBundle:Course')->findAll();

        return $this->render(':custom-admin-lists:courses_list.html.twig', array(
            'courses' => $courses
        ));
    }

    /**
     * @Route("/admin/app/course/{id}/attendees", name="attendees_list")
     */
    public function attendeesAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('AppBundle:Course')->findOneById($id);

        return $this->render(':custom-admin-lists:attendees_list.html.twig', array(
            'course' => $course,
        ));
    }
}
