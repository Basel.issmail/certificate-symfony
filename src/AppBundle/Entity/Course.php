<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $courseDate;

    /**
     * @ORM\Column(type="date")
     */
    private $certificateDate;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Attendee", mappedBy="courses")
     */
    private $attendees;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    private $urlID;

    public function __construct()
    {
        $this->attendees = new ArrayCollection();
        $this->urlID = base_convert(time(), 10, 36);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCourseDate(): ?\DateTimeInterface
    {
        return $this->courseDate;
    }

    public function setCourseDate(?\DateTimeInterface $courseDate): self
    {
        $this->courseDate = $courseDate;

        return $this;
    }

    public function getCertificateDate(): ?\DateTimeInterface
    {
        return $this->certificateDate;
    }

    public function setCertificateDate(\DateTimeInterface $certificateDate): self
    {
        $this->certificateDate = $certificateDate;

        return $this;
    }

    /**
     * @return Collection|Attendee[]
     */
    public function getAttendees(): Collection
    {
        return $this->attendees;
    }

    public function addAttendee(Attendee $attendee): self
    {
        if (!$this->attendees->contains($attendee)) {
            $this->attendees[] = $attendee;
            $attendee->addCourse($this);
        }

        return $this;
    }

    public function removeAttendee(Attendee $attendee): self
    {
        if ($this->attendees->contains($attendee)) {
            $this->attendees->removeElement($attendee);
            $attendee->removeCourse($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlID()
    {
        return $this->urlID;
    }

}
